package io.lokiraut.hotelo;

import lib.PatPeter.SQLibrary.Database;
import lib.PatPeter.SQLibrary.SQLite;
import org.bukkit.*;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.material.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by Lokio on 12/18/2015.
 */
public class Hotelo extends JavaPlugin implements Listener {

    Logger logger = null;
    private Database db;
    @Override
    public void onEnable() {
        logger = getLogger();
        logger.info("Plugin has been enabled!");
        Bukkit.getPluginManager().registerEvents(this, this);
        //enable logic
        db = new SQLite(logger,"[HoteloDB] ",this.getDataFolder().getAbsolutePath(),"HoteloDB",".db");
        try {
            db.open();
        }catch (Exception e){
            logger.info(e.getMessage());
        }
        //Initialize the Database
        try {
            if(!db.isTable("hotels")){
                db.query("CREATE TABLE hotels(id INTEGER PRIMARY KEY, name TEXT, managerID TEXT);");
            }
            if(!db.isTable("rooms")){
                db.query("CREATE TABLE rooms(id INTEGER PRIMARY KEY, roomID TEXT, hotelID INTEGER, occupied BIT, occupiedAt BIGINT);");
            }
            if(!db.isTable("signs")){
                db.query("CREATE TABLE signs(id INTEGER PRIMARY KEY, x INTEGER, y INTEGER, z INTEGER, worldID TEXT);");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for(World world:this.getServer().getWorlds()) {
            int oldScanners = 0;
            for (Entity entity : world.getEntities()) {
                if (entity instanceof ItemFrame) {
                    ItemFrame iem = (ItemFrame) entity;
                    if (iem.getCustomName() != null) {
                        if(iem.getCustomName().equals("CardScanner")) {
                            iem.remove();
                            oldScanners++;
                        }
                    }
                }
            }
            logger.info("In world " + world.getName() + ", " + oldScanners + " old scanner were removed!");
        }
        logger.info("Test");
        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        scheduler.scheduleSyncDelayedTask(this,new Runnable(){

            @Override
            public void run() {
                try {
                    ResultSet rs = db.query("SELECT * FROM signs;");
                    List<String> deletes = new ArrayList<String>();
                    while(rs.next()) {
                        UUID worldID = UUID.fromString(rs.getString(5));
                        World world = getServer().getWorld(worldID);
                        Block theBlock = world.getBlockAt(rs.getInt(2),rs.getInt(3),rs.getInt(4));
                        logger.info(theBlock.getType().toString());
                        if(theBlock.getState() instanceof org.bukkit.block.Sign) {
                            org.bukkit.block.Sign sign = (org.bukkit.block.Sign) theBlock.getState();
                            String[] linesParsed = handleSign(theBlock, ((Sign) theBlock.getState().getData()), sign.getLines(), null);
                            for (int i = 0; i < linesParsed.length; i++) {
                                sign.setLine(i, linesParsed[i]);
                            }
                            logger.info("Hopefully the thing did the stuff.");
                        }else{
                            deletes.add("DELETE FROM signs WHERE id=" + rs.getInt("id") + " AND x=" + rs.getInt("x") + " AND y=" + rs.getInt("y") + " AND z=" + rs.getInt("z") + " AND worldID=\"" + rs.getString("worldID") + "\";");
                        }
                        //TODO: Add logic to remove rows with invalid signs
                    }
                    rs.close();
                    for(String command: deletes){
                        db.query(command).close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        },3L);
    }
    @EventHandler
    public void onWorldLoad(WorldLoadEvent event){
        /**/
    }
    @Override
    public void onDisable() {
        logger.info("Plugin has been disabled!");
        //enable logic
        db.close();
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        String command = cmd.getName();
        if(command.equalsIgnoreCase("hotelo")){
            if(sender instanceof Player){
                Player player = (Player) sender;
                if(args.length > 0){
                    if(args[0].equals("rent")){
                        sender.sendMessage("§c[Hotelo]§r /hotelo rent <hotel id> <nights>");
                    }

                }else{

                    String[] messages = {"§c§lHotelo!","Type \"/hotelo help\" for commands!"};
                    sender.sendMessage(messages);
                }
                //world time = player.getWorld().getFullTime()
                // time = 1000 * in-game hours (not real hours)
            }else{
                sender.sendMessage(ChatColor.RED + "Only players can use Hotelo!");
            }
            return true;
        }
        return true;
    }

    public ItemFrame placeKeyCardScanner(Location signLoco,BlockFace facing, String hotel, Object cause){
        logger.info("scanner is beign placed?");
        World thisWorld = signLoco.getWorld();
        ItemFrame test = null;
        boolean failedToPlace = false;
        try {
            if (facing.equals(BlockFace.SOUTH)) {
                test = (ItemFrame) thisWorld.spawn(signLoco.add(0, 0, 2), ItemFrame.class);
            } else if (facing.equals(BlockFace.NORTH)) {
                test = (ItemFrame) thisWorld.spawn(signLoco.subtract(0, 0, 2), ItemFrame.class);
            } else if (facing.equals(BlockFace.EAST)) {
                test = (ItemFrame) thisWorld.spawn(signLoco.add(2, 0, 0), ItemFrame.class);
            } else if (facing.equals(BlockFace.WEST)) {
                test = (ItemFrame) thisWorld.spawn(signLoco.subtract(2, 0, 0), ItemFrame.class);
            } else {
                test = (ItemFrame) thisWorld.spawn(signLoco, ItemFrame.class);
            }
        }catch(NullPointerException e){
            logger.warning("Failed to place! D:");
            failedToPlace = true;
        }
        if(failedToPlace){
            for(Entity entity: thisWorld.getEntities()){
                if(entity instanceof ItemFrame){
                    ItemFrame itf = (ItemFrame) entity;
                    Location thisLoco = itf.getLocation();
                    if (thisLoco.getBlockX() == signLoco.getBlockX() && thisLoco.getBlockY() == signLoco.getBlockY() && thisLoco.getBlockZ() == signLoco.getBlockZ()) {
                        logger.info("Giving the existing itemframe for overwriting.");
                        test = itf;
                    }
                }else{
                    if(cause != null){
                        if(cause instanceof Player){
                            Player c = (Player) cause;
                            c.sendMessage("Obstruction on other side of wall. Cannot place card scanner.");

                        }
                    }
                    return null;
                }
            }

        }
        logger.info("All is well I guess.");
        test.setFacingDirection(facing, true);
        test.setItem(new ItemStack(Material.PRISMARINE_SHARD));
        test.setMetadata("CardScanner", new FixedMetadataValue(this, hotel));
        test.setCustomName("CardScanner");
        test.setCustomNameVisible(true);
        return test;
    }
    public String[] handleSign(Block baseBlock, Sign thisSign, String[] lines, Object cause){
        boolean signError = false;
        if(lines[0].replaceAll(" ","").equalsIgnoreCase("[hotelo]")){
            lines[0] = "§1[Hotelo]";
        }else if(!lines[0].equals("§1[Hotelo]")){
            return lines;
        }
        if(!baseBlock.getType().equals(Material.WALL_SIGN)){
            lines[1] = lines[2] = "";
            lines[3] = "§4Place on a wall!";
            signError = true;
        }


        /*
        0 = hotelo
        1 = operation
        2 = value
        3 = programatically set thing
         */
        String[] operation = lines[1].split(" ");
        if(operation.length > 0) {
            if (operation[0].equalsIgnoreCase("scanner")) {
                if (operation.length > 1){
                    try {
                        if (operation[1].equalsIgnoreCase("dealer")) {
                            ItemFrame dealerFrame = placeKeyCardScanner(baseBlock.getLocation(), thisSign.getFacing().getOppositeFace(), "Example Hotel", cause);
                            dealerFrame.setMetadata("scannerType", new FixedMetadataValue(this, "dealer"));
                        } else if (operation[1].equalsIgnoreCase("room")) {
                            ItemFrame roomFrame = placeKeyCardScanner(baseBlock.getLocation(), thisSign.getFacing().getOppositeFace(), "Example Hotel", cause);
                            roomFrame.setMetadata("scannerType", new FixedMetadataValue(this, "room"));
                            roomFrame.setMetadata("roomNumber", new FixedMetadataValue(this, "100"));
                            lines[3] = "§4LOCKED";
                        } else if (operation[1].equalsIgnoreCase("door")) {
                            ItemFrame doorFrame = placeKeyCardScanner(baseBlock.getLocation(), thisSign.getFacing().getOppositeFace(), "Example Hotel", cause);
                            doorFrame.setMetadata("scannerType", new FixedMetadataValue(this, "door"));
                            doorFrame.setMetadata("doorID", new FixedMetadataValue(this, "Pool"));
                        } else {
                            signError = true;
                            lines[1] = lines[2] = "";
                            lines[3] = "§4ERROR!";
                            if(cause instanceof Player) {
                                ((Player) cause).sendMessage(ChatColor.RED + "[Hotelo]§r Scanner must be a dealer, room or door!");
                            }
                        }
                    }catch(Exception e){
                        logger.warning(e.getMessage());
                        signError = true;
                        lines[1] = lines[2] = "";
                        lines[3] = "§4ERROR!";
                    }
                }else{
                    signError = true;
                    if(cause instanceof Player) {
                        ((Player) cause).sendMessage(ChatColor.RED + "[Hotelo]§r Usage: scanner <dealer/room/door>");
                    }
                    lines[1] = lines[2] = "";
                    lines[3] = "§4ERROR!";
                }
            }else if(operation[0].equalsIgnoreCase("room")){
                if(!lines[2].replaceAll(" ","").equals("")){
                    lines[3] = "§3Vacant";
                }else{
                    signError = true;
                    if(cause instanceof Player) {
                        ((Player) cause).sendMessage(ChatColor.RED + "[Hotelo]§r Usage: Rooms require an identifier!");
                    }
                    lines[1] = lines[2] = "";
                    lines[3] = "§4ERROR!";
                }
            }else if(operation[0].equalsIgnoreCase("door")){

            }else if(operation[0].equalsIgnoreCase("dispense")){

            }else{
                signError = true;
                lines[1] = lines[2] = "";
                lines[3] = "§4ERROR!";
                if(cause instanceof Player) {
                    ((Player) cause).sendMessage(ChatColor.RED + "[Hotelo]§r Please type a valid operation!");
                }
            }
        }
        if(!signError&&cause instanceof Player){
            try {
                Location bl = baseBlock.getLocation();
                logger.info(bl.getWorld().getUID().toString());
                db.query("INSERT INTO signs VALUES(NULL," + bl.getBlockX() + "," + bl.getBlockY() + "," + bl.getBlockZ() + ",\"" + bl.getWorld().getUID().toString() + "\");");
                logger.info("Added sign to database.");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return lines;
    }
    @EventHandler
    public void onSignPlace(SignChangeEvent event){
        Sign thisSign = (Sign) event.getBlock().getState().getData();
        String[] linesParsed = handleSign(event.getBlock(),thisSign,event.getLines(),event.getPlayer());
        for(int i = 0; i < linesParsed.length; i++ ){
            event.setLine(i,linesParsed[i]);
        }
    }

    @EventHandler
    public void onEntityInteract(PlayerInteractEntityEvent event){
        //keycard and stuff
        /*event.getRightClicked().setMetadata("CardScanner",new FixedMetadataValue(this,"ExampleHotel"));
        event.getRightClicked().getLocation().add(0,0,-1).getBlock().setType(Material.STONE);
        */
        Entity rc = event.getRightClicked();
        Player cause = event.getPlayer();
        if(rc instanceof ItemFrame){
            if(rc.getCustomName() != null) {
                if (rc.getCustomName().equals("CardScanner")) {
                    if (cause.getItemInHand().getType().equals(Material.PRISMARINE_SHARD) && rc.getMetadata("scannerType").get(0).asString().equals("dealer")) {
                        ItemMeta im = cause.getItemInHand().getItemMeta();
                        im.setDisplayName("§R§e§L" + cause.getDisplayName() + "'s Key Card");
                        List<String> lore = new ArrayList<String>();
                        lore.add("§f§oKey Card for use at Example Hotel");
                        lore.add("");
                        lore.add("§r§cUnlocks:");
                        lore.add("§r Room 321");
                        lore.add("§r Front Door");
                        lore.add("§r Pool Door");
                        im.setLore(lore);
                        cause.getItemInHand().setItemMeta(im);
                        cause.sendMessage("Here's your Key Card!");
                    }
                    event.setCancelled(true);
                }
            }
        }

    }
    @EventHandler
    public void onItemFrameEdit(EntityDamageEvent event){
        //logger.info("fired damage event");
        if(event.getEntity() instanceof ItemFrame){
            ItemFrame iem = (ItemFrame) event.getEntity();
            if(iem.getCustomName() != null){
                if(iem.getCustomName().equals("CardScanner")){
                    event.setCancelled(true);
                }
            }
        }
    }
    @EventHandler
    public void onSignDied(BlockBreakEvent event){
        if(event.getBlock().getState() instanceof org.bukkit.block.Sign){
            org.bukkit.block.Sign sign = (org.bukkit.block.Sign) event.getBlock().getState();
            if(sign.getLine(0).equals("§1[Hotelo]")){
                Location signLoco = sign.getLocation();
                try {
                    ResultSet rs = db.query("DELETE FROM signs WHERE x=" + signLoco.getBlockX()
                            + " AND y=" + signLoco.getBlockY()
                            + " AND z=" + signLoco.getBlockZ()
                            + " AND worldID=\""
                            + signLoco.getWorld().getUID().toString() + "\"");
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
