package org.sparkstudios.wildfire.chat;

import javafx.scene.control.PaginationBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;
import org.sparkstudios.wildfire.core.WildFireCore;

import java.util.*;
import java.util.logging.Logger;

/**
 * Created by Lokio on 12/20/2015.
 */
public class WildFireCommands extends JavaPlugin implements Listener {
    Logger logger =  null;
    WildFireCore core;
    @Override
    public void onEnable() {
        logger = getLogger();
        Bukkit.getPluginManager().registerEvents(this, this);
        logger.info("Enabled WildFireCommands");
        try {
            core = (WildFireCore) Bukkit.getPluginManager().getPlugin("WildFireCore");
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
        if(cmd.getName().equalsIgnoreCase("spawn")){
            if(args.length == 0){
                if(sender instanceof Player){
                    sender.sendMessage("Teleporting you to spawn...");
                    Player s = (Player) sender;
                    s.teleport(s.getWorld().getSpawnLocation());
                }else{
                    sender.sendMessage("You must be a player to execute /spawn!");
                }
            }else{
                if(sender instanceof Player){
                    Player s = (Player) sender;
                    Player target = s.getServer().getPlayer(args[0]);
                    if(target != null){
                        target.sendMessage(s.getDisplayName() + " sent you to spawn!");
                        Location SpawnLoco = target.getWorld().getSpawnLocation();
                        target.teleport(SpawnLoco);
                    }else{
                        s.sendMessage("Player \"" + args[0] + "\" does not exist.");
                    }
                }else{
                    Player target = getServer().getPlayer(args[0]);
                    if(target != null){
                        target.sendMessage("A non-user sent you to spawn!");
                        target.teleport(target.getWorld().getSpawnLocation());
                    }else{
                        sender.sendMessage("Player \"" + args[0] + "\" does not exist.");
                    }
                }
            }
            return true;
        }else if(cmd.getName().equalsIgnoreCase("setspawn")){
            if(sender instanceof Player){
                Player ps = (Player) sender;
                Location loco = ps.getLocation();
                ps.getWorld().setSpawnLocation(loco.getBlockX(),loco.getBlockY(),loco.getBlockZ());
                sender.sendMessage("Set spawn position!");
            }else{
                sender.sendMessage("Only players can set spawn!");
            }
            return true;
        }else if(cmd.getName().equalsIgnoreCase("warp")){
            if (sender instanceof Player) {
                Player ps = (Player) sender;
                if(args.length > 0) {
                    if(args[0].equalsIgnoreCase("list")) {
                        Object[] ks = {};
                        try {
                            ConfigurationSection l = core.config.getConfigurationSection("warps");
                            ks = l.getKeys(false).toArray();
                        }catch(Exception e){

                        }
                        ps.sendMessage("Listing " + ks.length + ((ks.length!=1)?" warps...":" warp..."));
                        for(Object warp: ks){
                            ps.sendMessage(" §a- §r" + warp.toString());
                        }
                    }else if (core.config.isSet("warps." + args[0].toLowerCase())) {
                        Location teleportGoal = core.getLocationConfig("warps." + args[0].toLowerCase());
                        ps.teleport(teleportGoal);
                        ps.sendMessage("Teleporting you to warp §c" + args[0] + "§r.");
                    } else{
                        ps.sendMessage("§cError§r: Warp §c" + args[0].toLowerCase() + "§r does not exist!");
                    }
                }  else {
                    ps.sendMessage("Type /warp list for a list of warps.");
                    ps.sendMessage("Type /warp <warpname> to go to a warp.");
                }
            } else {
                sender.sendMessage("Must be a player!");
            }
            return true;
        }else if(cmd.getName().equalsIgnoreCase("setwarp")){
            if(sender instanceof Player){
                Player ps = (Player) sender;
                Location loco = ps.getLocation();
                String fixed = args[0].toLowerCase().replace(".","_");
                core.putLocationConfig("warps." + fixed,loco);
                core.saveConfig();
                ps.sendMessage("Warp §c" + fixed + "§r has been set to your current location.");
            }else{
                sender.sendMessage("Must be a player!");
            }
            return true;
        }else if(cmd.getName().equalsIgnoreCase("delwarp")){
            if(args.length > 0) {
                if (core.config.isSet("warps." + args[0])) {
                    core.config.set("warps." + args[0], null);
                    core.saveConfig();
                    sender.sendMessage("Successfully removed warp §c" + args[0] + "§r.");
                }else{
                    sender.sendMessage("§cError§r: §c" + args[0] + "§r does not exist!");
                }
            }else{
                sender.sendMessage("§cError§r: We need a warp name to delete a warp. >:(");
            }
            return true;
        }else if(cmd.getName().equalsIgnoreCase("home")){
            if(sender instanceof Player){
                Player sndr = (Player) sender;
                if(core.config.isSet("users." + sndr.getUniqueId().toString())){
                    if(core.config.isSet("users." + sndr.getUniqueId().toString() + ".home")){
                        Location thisloco = core.getLocationConfig("users." + sndr.getUniqueId().toString() + ".home");
                        sender.sendMessage("Sending you home.");
                        sndr.teleport(thisloco);
                    }else{
                        sender.sendMessage("§cError§r: Must have a home set first!");
                    }
                }else{
                    sender.sendMessage("§cError§r: Must have a home set first!");
                }
            }else{
                sender.sendMessage("§cError§r: Must be player to go home.");
            }
            return true;
        }else if(cmd.getName().equalsIgnoreCase("sethome")){
            if(sender instanceof Player){
                Player sndr = (Player) sender;
                core.assureUserInConfig(sndr);
                core.putLocationConfig("users." + sndr.getUniqueId().toString() + ".home", sndr.getLocation());
                core.saveConfig();
                sender.sendMessage("Your home has been successfully set.");
            }else{
                sender.sendMessage("§cError§r: Must be player to sethome.");
            }
            return true;
        }else if(cmd.getName().equalsIgnoreCase("nick")||cmd.getName().equalsIgnoreCase("setnick")){
            if(core.attachedModules.get("WildFireChat")){
                if(sender instanceof Player) {
                    WildFireChat wfc = (WildFireChat) core.modules.get("WildFireChat");
                    if(args.length > 0) {
                        String doNick = "";
                        for(String part: args){
                            doNick += part + " ";
                        }
                        doNick = doNick.substring(0,doNick.length()-1);
                        if(args.length == 1){
                            doNick = args[0];
                        }
                        wfc.setNickname((Player) sender, doNick);
                        sender.sendMessage("[" + core.branding + "] Set nickname to \"" + wfc.formatMessage(doNick) + "§r\"!");
                    }else{
                        sender.sendMessage("[" + core.branding + "] Removed Nickname!");
                        wfc.setNickname((Player) sender, null);
                    }
                }else{
                    sender.sendMessage("§cError§r: Must be player to assign nicknames.");
                }
            }else{
                sender.sendMessage("§cError§r: Please enable WildFireChat to use nicknames.");
            }

            return true;
        }else if(cmd.getName().equalsIgnoreCase("me")){
            String playerName = "";
            WildFireChat wfc;
            if(core.attachedModules.get("WildFireChat")) {
                wfc = (WildFireChat) core.modules.get("WildFireChat");
                if (sender instanceof Player) {
                    Player plr = (Player) sender;
                    if (wfc.nicknames.containsKey(plr.getUniqueId().toString())) {
                        playerName = wfc.nicknames.get(plr.getUniqueId().toString());
                    } else {
                        playerName = sender.getName();
                    }
                } else {
                    playerName = sender.getName();
                }


                String append = "";
                for (int p = 0; p < args.length; p++) {
                    append += args[p] + " ";
                }
                append = append.substring(0, append.length() - 1);
                if (args[0].contains("'s") || args[0].contains("`s")) {
                    sender.getServer().broadcastMessage("§a* §f" + wfc.formatMessage(playerName) + "§f" + append);
                } else {
                    sender.getServer().broadcastMessage("§a* §f" + wfc.formatMessage(playerName) + "§f " + append);
                }
            }else{
                sender.sendMessage("§cError§r: Please install WildFire Chat to use /me!");
            }
            return true;
        }else if(cmd.getName().equalsIgnoreCase("tp")){
            return true;
        }else if(cmd.getName().equalsIgnoreCase("tphere")){
            return true;
        }else if(cmd.getName().equalsIgnoreCase("holo")){
            if(args.length > 0) {
                if(args[0].equalsIgnoreCase("create")||args[0].equalsIgnoreCase("add")) {
                    if (sender instanceof Player) {
                        if (core.attachedModules.get("WildFireChat")) {
                            if(args.length > 2) {
                                WildFireChat wfc = (WildFireChat) core.modules.get("WildFireChat");
                                Player send = (Player) sender;
                                ArmorStand as = (ArmorStand) send.getWorld().spawnEntity(send.getLocation().clone().subtract(0, 0, 0), EntityType.ARMOR_STAND);
                                String holoString = "";
                                for (int n = 2; n < args.length; n++) {
                                    holoString += args[n] + " ";
                                }
                                holoString = holoString.substring(0, holoString.length() - 1);
                                as.setCustomName("§r" + wfc.formatMessage(holoString));
                                as.setCustomNameVisible(true);
                                as.setGravity(false);
                                as.setVisible(false);
                                as.setSmall(true);
                                as.setBasePlate(false);
                                as.setLeashHolder(send);
                                core.config.set("holograms." + args[1] + ".uid",as.getUniqueId().toString());
                                core.config.set("holograms." + args[1] + ".world",as.getWorld().getUID().toString());
                                core.saveConfig();
                                sender.sendMessage("made a hologram buddi");
                            }else{
                                sender.sendMessage("/holo create <id> <text>");
                            }
                        }else{
                            sender.sendMessage("WildFireChat is required for holograms.");
                        }
                    }else{
                        sender.sendMessage("Only players can create holograms.");
                    }
                }else if(args[0].equalsIgnoreCase("remove")||args[0].equalsIgnoreCase("delete")){
                    if(args.length >= 1){
                        if(core.config.isSet("holograms." + args[1])){
                            World nuker = getServer().getWorld(UUID.fromString(core.config.getString("holograms." + args[1] + ".world")));
                            List<Entity> ents = nuker.getEntities();
                            for(Entity target: ents){
                                if(target.getUniqueId().toString().equals(core.config.getString("holograms." + args[1] + ".uid"))){
                                    target.remove();
                                }
                            }
                            core.config.set("holograms." + args[1],null);
                            core.saveConfig();
                            sender.sendMessage("removed hologram §c" + args[1]);
                        }else{
                            sender.sendMessage("didn't exist anyway, bud.");
                        }
                    }
                }else if(args[0].equalsIgnoreCase("edit")) {
                    if(args.length >= 1){
                        if (core.attachedModules.get("WildFireChat")) {
                            WildFireChat wfc = (WildFireChat) core.modules.get("WildFireChat");
                            if (core.config.isSet("holograms." + args[1])) {
                                World nuker = getServer().getWorld(UUID.fromString(core.config.getString("holograms." + args[1] + ".world")));
                                List<Entity> ents = nuker.getEntities();
                                for (Entity target : ents) {
                                    if (target.getUniqueId().toString().equals(core.config.getString("holograms." + args[1] + ".uid"))) {
                                        String holoString = "";
                                        for (int n = 2; n < args.length; n++) {
                                            holoString += args[n] + " ";
                                        }
                                        holoString = holoString.substring(0, holoString.length() - 1);
                                        target.setCustomName(wfc.formatMessage(holoString));
                                    }
                                }
                                sender.sendMessage("edited  hologram §c" + args[1]);
                            } else {
                                sender.sendMessage("didn't exist, bud.");
                            }
                        }else{
                            sender.sendMessage("WildFireChat required to edit holograms!");
                        }
                    }else{
                        sender.sendMessage("/holo edit <id> <text>");
                    }
                }else if(args[0].equalsIgnoreCase("list")) {
                    if(args.length >= 1){
                        if (core.config.isSet("holograms")) {
                            sender.sendMessage("Listing hologram ids...");
                            Set<String> ss = core.config.getConfigurationSection("holograms").getKeys(false);
                            for(String s: ss){
                                sender.sendMessage("§a- §r" + s);
                            }
                        } else {
                            sender.sendMessage("Either there are no holograms or something is horribly wrong.");
                        }
                    }else{
                        sender.sendMessage("/holo edit <id> <text>");
                    }
                }else {
                    sender.sendMessage("Invalid subcommand of holo: §c" + args[0]);
                }
            }else{
                sender.sendMessage("/holo <create/remove/edit> <id> [text]");
            }
            return true;
        }
        return false;
    }

    @EventHandler
    public void onInteractHolo(PlayerInteractAtEntityEvent event){

        if(event.getRightClicked() instanceof ArmorStand){
            ArmorStand as = (ArmorStand) event.getRightClicked();
            if(as.getCustomName() != null){
                if(as.getCustomName().substring(0,2).equals("§r")){
                    event.setCancelled(true);
                }
            }
        }
    }

}
