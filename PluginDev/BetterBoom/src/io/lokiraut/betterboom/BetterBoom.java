package io.lokiraut.betterboom;

import net.minecraft.server.v1_8_R3.Blocks;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.logging.Logger;

/**
 * Created by Lokio on 12/20/2015.
 */
public class BetterBoom extends JavaPlugin implements Listener {
    Logger logger = null;
    Float original = 0F;
    @Override
    public void onEnable() {
        logger = getLogger();
        Bukkit.getPluginManager().registerEvents(this, this);
        logger.info("Better Boom now enabled!");

        try {
            Field field = net.minecraft.server.v1_8_R3.Block.class.getDeclaredField("strength");
            field.setAccessible(true);
            original = field.getFloat(Blocks.OBSIDIAN);
            field.setFloat(Blocks.OBSIDIAN,1.0F);
            logger.info("set obsidian to " + field.getFloat(Blocks.OBSIDIAN));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        try {
            Field field = net.minecraft.server.v1_8_R3.Block.class.getDeclaredField("strength");
            field.setAccessible(true);
            logger.info("Setting obsidian to " + original);
            field.setFloat(Blocks.OBSIDIAN,original);
            logger.info("Set obsidian.");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onRight(PlayerInteractEvent event){

        if(event.getItem() != null){
            if(event.getItem().getType().equals(Material.TNT)) {
                Player cause = event.getPlayer();
                if (!event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
                    return;
                }
                if (!cause.getGameMode().equals(GameMode.CREATIVE)) {
                    if (cause.getItemInHand().getAmount() == 1) {
                        cause.getInventory().remove(cause.getItemInHand());
                    } else {
                        cause.getItemInHand().setAmount(cause.getItemInHand().getAmount() - 1);
                    }
                }
                Location playerLoco = cause.getLocation();
                World world = playerLoco.getWorld();
                try {
                    TNTPrimed pt = (TNTPrimed) world.spawnEntity(playerLoco.add(0, 1, 0), EntityType.PRIMED_TNT);
                    pt.setVelocity(playerLoco.getDirection());
                    event.setCancelled(true);
                }catch(Exception e){

                }

            }
        }
    }
    @EventHandler
    public void onTNTBoom(EntityExplodeEvent event){

        Material[] noFly = {
                Material.SNOW,
                Material.RED_ROSE,
                Material.TORCH,
                Material.YELLOW_FLOWER,
                Material.WATER_LILY,
                Material.CROPS,
                Material.GLASS,
                Material.STAINED_GLASS,
                Material.LONG_GRASS,
                Material.CAKE,
                Material.CARROT,
                Material.POTATO,
                Material.THIN_GLASS,
                Material.STAINED_GLASS,
                Material.PORTAL,
                Material.ENDER_PORTAL,
                Material.WEB,
                Material.DOUBLE_PLANT
        };
        Material[] convertToDirt = {
                Material.GRASS,
                Material.SOIL

        };
        Material[] oreDrop = {
                Material.COAL_ORE,
                Material.DIAMOND_ORE,
                Material.EMERALD_ORE,
                Material.GLOWING_REDSTONE_ORE,
                Material.REDSTONE_ORE,
                Material.LAPIS_ORE
        };
        //event.
        for(Block block: event.blockList()){
            Material fallMaterial = block.getType();
            Byte fallMaterialData = block.getData();
            Location fallLocation = block.getLocation();
            World thisWorld = fallLocation.getWorld();
            float blastSize = 1;
            Vector boomVel = new Vector(((Math.random()*blastSize) + ((Math.random()*blastSize) * -1)),blastSize,((Math.random()*blastSize) + ((Math.random()*blastSize) * -1)));
            boolean fly = true;
            boolean dropNatural = false;
            if(fallMaterial.equals(Material.TNT)){
                block.setType(Material.AIR);
                TNTPrimed tp = (TNTPrimed) thisWorld.spawnEntity(fallLocation,EntityType.PRIMED_TNT);
                //tp.setVelocity(boomVel.divide(new Vector(4,4,4)));
                tp.setFuseTicks(((int)(Math.random()*20)) + 10);
                fly = false;
            }
            if(fallMaterial.equals(Material.STONE)&&Math.random()*100 > 40){
                fallMaterial = Material.COBBLESTONE;
            }
            for(Material mat: convertToDirt){
                if(fallMaterial.equals(mat)){
                    fly = false;
                }
            }
            for(Material mat: oreDrop){
                if(fallMaterial.equals(mat)){
                    fly = false;
                }
            }
            for(Material mat: noFly){
                if(fallMaterial.equals(mat)){
                    fly = false;
                }
            }
            if(fly) {
                block.setType(Material.AIR);
                thisWorld.spawnFallingBlock(fallLocation, fallMaterial, fallMaterialData).setVelocity(boomVel);
            }else{
                if(dropNatural){
                    block.breakNaturally();
                }
            }
        }
        //event.setCancelled(true);
    }
}
