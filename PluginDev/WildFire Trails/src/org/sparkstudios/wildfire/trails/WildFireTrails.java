package org.sparkstudios.wildfire.trails;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;
import com.darkblade12.particleeffect.ParticleEffect;
import org.sparkstudios.wildfire.core.WildFireCore;

import java.util.logging.Logger;

/**
 * Created by Lokio on 12/20/2015.
 */
public class WildFireTrails extends JavaPlugin implements Listener {
    Logger logger = null;
    WildFireCore core;
    @Override
    public void onEnable() {
        logger = this.getLogger();
        Bukkit.getPluginManager().registerEvents(this, this);
        logger.info("WildFireTrails enabled!");
        logger.info("Coming Soon: Per-User Trails and Commands?");
        try {
            core = (WildFireCore) Bukkit.getPluginManager().getPlugin("WildFireCore");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event){
        Location oldLoco = event.getFrom();
        Location newLoco = event.getTo();
        if(oldLoco.getX() == newLoco.getX()&&oldLoco.getY() == newLoco.getY()&&oldLoco.getZ() == newLoco.getZ()){
            return;
        }
        //oldLoco.subtract(oldLoco.getDirection().getX(),0,oldLoco.getDirection().getY());
        Vector vel = new Vector((Math.random() + (Math.random() * -1))/4,1,(Math.random() + (Math.random() * -1))/4);
        /*ParticleEffect.FLAME.display(vel, ((float)Math.random()/100) + 0.02f, oldLoco, 1000);
        ParticleEffect.FLAME.display(vel, ((float)Math.random()/100) +0.05f, oldLoco, 1000);
        ParticleEffect.FLAME.display(vel, ((float)Math.random()/100) +0.08f, oldLoco, 1000);*/

        float circleFill = 10;
        float circleCount = 5;
        for(int y = 0; y < circleCount; y++) {
            for (int i = 0; i < circleFill; i++) {
                int random1 = (int) (Math.random() * 255);
                int random2 = (int) (Math.random() * 255);
                int random3 = (int) (Math.random() * 255);
                Vector shift = new Vector(Math.asin(i*(360/circleFill)),y * (2/(circleCount-1)),Math.acos(i*(360/circleFill)));
                //logger.info(shift.toString());
                Location ourLocation = oldLoco.clone().add(shift);
                //ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(random1, random2, random3), ourLocation, 100);
            }
        }
        //newLoco.getWorld().
        //ParticleEffect.SMOKE_LARGE.display(new Vector(0,1,0), 0.1f, oldLoco, 10);
    }
    public void commandHelper(CommandSender sender, String[] args){

    }
}
