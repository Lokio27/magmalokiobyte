package org.sparkstudios.wildfire.chat;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.sparkstudios.wildfire.core.WildFireCore;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by Lokio on 12/20/2015.
 */
public class WildFireChat extends JavaPlugin implements Listener {
    Logger logger =  null;
    WildFireCore core;
    public Map<String, String> nicknames;
    @Override
    public void onEnable() {
        logger = getLogger();
        Bukkit.getPluginManager().registerEvents(this, this);
        logger.info("Enabled WildFireChat");
        try {
            core = (WildFireCore) Bukkit.getPluginManager().getPlugin("WildFireCore");
        }catch(Exception e){
            e.printStackTrace();
        }
        loadNicknames();
    }

    public void loadNicknames(){
        nicknames = new HashMap<>();
        if(core.config.isSet("users")) {
            Set<String> i = core.config.getConfigurationSection("users").getKeys(false);
            for(Object uuid: i.toArray()){
                String key = (String) uuid;
                ConfigurationSection userConf =  core.config.getConfigurationSection("users." + key);
                if(userConf.isString("nickname")){
                    nicknames.put(key, userConf.getString("nickname"));
                    logger.info("populating the nicknames!");
                }
            }
        }
    }

    public void setNickname(Player player, String nickname){
        core.config.set("users." + player.getUniqueId().toString() + ".nickname",nickname);
        core.saveConfig();
        nicknames.put(player.getUniqueId().toString(),nickname);
    }
    public String formatMessage(String message){
        return ChatColor.translateAlternateColorCodes('&',message);
    }
    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        if(nicknames.containsKey(event.getPlayer().getUniqueId().toString())) {
            if(nicknames.get(event.getPlayer().getUniqueId().toString()) == null){
                event.setFormat("§r%1$s§a:§r %2$s");
            }else{
                event.setFormat("§r" + formatMessage(nicknames.get(event.getPlayer().getUniqueId().toString())) + "§a:§r %2$s");
            }
        }else{
            event.setFormat("§r%1$s§a:§r %2$s");
        }
        event.setMessage(formatMessage(event.getMessage()));
        for (Player player: getServer().getOnlinePlayers()) {
            player.playSound(player.getLocation(), Sound.NOTE_STICKS, 0.5f, 1f);
        }
    }
    @EventHandler
    public void onSignChange(SignChangeEvent event){
        //do stuff
        String[] sl = event.getLines();
        for(int i = 0; i < sl.length; i++){
            event.setLine(i,formatMessage(sl[i]));
        }
    }
    public void commandHelper(CommandSender sender, String[] args){
        core.sendDivider(sender);
        sender.sendMessage("Testing123");
        core.sendDivider(sender);
    }
}
