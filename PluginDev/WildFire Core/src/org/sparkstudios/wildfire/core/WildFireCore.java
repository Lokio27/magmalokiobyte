package org.sparkstudios.wildfire.core;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;
import org.sparkstudios.wildfire.chat.WildFireChat;
import org.sparkstudios.wildfire.trails.WildFireTrails;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by Lokio on 12/22/2015.
 */
public class WildFireCore extends JavaPlugin implements Listener {
    Logger logger;

    public File configFile;
    public FileConfiguration config;

    public Map<String, Plugin> modules;
    public Map<String, Boolean> attachedModules;
    private Map<String, String> argToModule;

    public String branding;

    @Override
    public void onEnable() {
        // === Begin General Assignments === //
        PluginManager pm = Bukkit.getPluginManager();
        // === End General Assignments ===== //

        // === Begin Class Member Assignments === //
        this.logger = this.getLogger();

        this.modules = new HashMap<>();
        this.attachedModules = new HashMap<>();
        this.argToModule = new HashMap<>();

        this.branding = "§c§lWild§6§lFire§r";

        this.config = this.getConfig();

        try {
            if (!this.getDataFolder().exists()) {
                this.getDataFolder().mkdirs();
            }

            this.configFile = new File(this.getDataFolder(), "config.yml");

            if (!this.configFile.exists()) {
                this.logger.info("WildFire Core config file not found. Will create.");
                this.saveDefaultConfig();
            } else {
                this.logger.info("WildFire Core config file found. Will load.");
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        // === End Class Member Assignments ===== //

        // === Begin General Initialization Logic === //
        this.logger.info("Hello from WildFire Core!");

        this.attachedModules.put("WildFireChat", (pm.getPlugin("WildFireChat") != null));
        this.attachedModules.put("WildFireTrails", (pm.getPlugin("WildFireTrails") != null));
        this.attachedModules.put("WildFirePermissions", (pm.getPlugin("WildFirePermissions") != null));
        this.attachedModules.put("WildFireProtect", (pm.getPlugin("WildFireProtect") != null));
        this.attachedModules.put("WildFireCommands", (pm.getPlugin("WildFireCommands") != null));

        this.argToModule.put("chat", "WildFireChat");
        this.argToModule.put("trails", "WildFireTrails");
        this.argToModule.put("permissions", "WildFirePermissions");
        this.argToModule.put("protect", "WildFireProtect");

        for (String key: this.attachedModules.keySet()) {
            if (this.attachedModules.get(key)) {
                this.modules.put(key, pm.getPlugin(key));
            }
        }

        pm.registerEvents(this, this);
        // === End General Initialization Logic ===== //
    }

    public void assureUserInConfig(Player player) {
        this.logger.info("WildFireCore.assureUserInConfig invoked!");

        this.config.set("users." + player.getUniqueId().toString() + ".lastKnownName", player.getDisplayName());

        this.saveConfig();
    }

    public void putLocationConfig(String path, Location loc) {
        this.config.set(path + ".x", loc.getX());
        this.config.set(path + ".y", loc.getY());
        this.config.set(path + ".z", loc.getZ());
        this.config.set(path + ".rot.x", loc.getDirection().getX());
        this.config.set(path + ".rot.y", loc.getDirection().getY());
        this.config.set(path + ".rot.z", loc.getDirection().getZ());
        this.config.set(path + ".world", loc.getWorld().getUID().toString());
    }

    public Location getLocationConfig(String path) {
        if (this.config.isSet(path)) {
            World world = this.getServer().getWorld(UUID.fromString(config.getString(path + ".world")));

            double x = this.config.getDouble(path + ".x");
            double y = this.config.getDouble(path + ".y");
            double z = this.config.getDouble(path + ".z");

            double rotX = (double) this.config.get(path + ".rot.x");
            double rotY = (double) this.config.get(path + ".rot.y");
            double rotZ = (double) this.config.get(path + ".rot.z");

            Location location = new Location(world, x, y, z).setDirection(new Vector(rotX, rotY, rotZ));

            return location;
        }

        return null;
    }

    public Object[] arrayAppend(Object[] originalArray, Object item, int index) {
        // Since we're "appending" a single item to the array,
        //  we know that the new array will need to be one item larger than the original.
        // We can set that up here.
        Object[] newArray = new Object[originalArray.length + 1];

        // Here, we're just copying the original array into the new array.
        System.arraycopy(originalArray, 0, newArray, 0, index);

        // Here, we'll go ahead and assign the last item in the array to the desired item.
        newArray[index] = item;

        // And that's it. Return the new array to the caller!
        return newArray;
    }

    public void sendDivider(CommandSender sender) {
        sender.sendMessage("§a-----------------------------------");
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent evt) {
        this.assureUserInConfig(evt.getPlayer());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        boolean hasParams = (args.length > 0);

        if (command.getName().equalsIgnoreCase("wildfire") || command.getName().equalsIgnoreCase("wf")) {
            if (hasParams) {
                switch (args[0]) {
                    case "help":
                        this.sendDivider(sender);
                        sender.sendMessage(this.branding + " Help");
                        this.sendDivider(sender);
                        break;

                    case "reload":
                        reloadConfig();
                        sender.sendMessage(branding + " has been reloaded.");
                        break;

                    default:
                        String key = this.argToModule.get(args[0].toLowerCase());

                        if (key != null && this.attachedModules.containsKey(key)) {
                            if (this.attachedModules.get(key)) {
                                Plugin mod = this.modules.get(key);

                                if (mod instanceof WildFireTrails) {
                                    WildFireTrails trailsInstance = (WildFireTrails) mod;

                                    String[] modArgs = {};

                                    for (int i = 1; i < args.length; i++) {
                                        modArgs = (String[]) arrayAppend(modArgs, args[i], i - 1);
                                    }

                                    trailsInstance.commandHelper(sender, modArgs);
                                } else if (mod instanceof WildFireChat) {
                                    WildFireChat chatInstance = (WildFireChat) mod;

                                    String[] modArgs = {};

                                    for (int i = 1; i < args.length; i++) {
                                        modArgs = (String[]) arrayAppend(modArgs, args[i], i - 1);
                                    }

                                    chatInstance.commandHelper(sender, modArgs);
                                }
                            } else {
                                sender.sendMessage("§cError§r: Module \"" + key + "\" is not loaded or installed!");
                            }
                        } else {
                            sender.sendMessage("§cError§r: \"" + args[0] + "\" is an invalid argument! Type /wildfire help for help!");
                        }
                        break;
                }
            } else {
                sendDivider(sender);
                sender.sendMessage(this.branding + " v" + this.getDescription().getVersion());
                sender.sendMessage("Type \"/wildfire help\" for more commands.");
                sender.sendMessage("Modules: ");

                for (String modKey: this.attachedModules.keySet()) {
                    String prefix = (this.attachedModules.get(modKey) == true ? "  §a+§r " : "  §c-§r ");

                    sender.sendMessage(prefix + modKey);
                }
                sendDivider(sender);
            }

            return true;
        }

        return false;
    }
}
